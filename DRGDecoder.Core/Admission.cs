﻿using DRGDecoder.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.Core
{
    public class Admission
    {
        private int _yearAge;

        public int YearAge
        {
            get { return _yearAge; }
            set { _yearAge = value; }
        }

        private Nullable<int> _daysAge;

        public Nullable<int> DaysAge
        {
            get { return _daysAge; }
            set { _daysAge = value; }
        }
        private int _sex;

        public int Sex
        {
            get { return _sex; }
            set { _sex = value; }
        }
        private Nullable<int> _admissionWeight;

        public Nullable<int> AdmissionWeight
        {
            get { return _admissionWeight; }
            set { _admissionWeight = value; }
        }

        private Diagnostic _mainDiagnostic;

        public Diagnostic MainDiagnostic
        {
            get { return _mainDiagnostic; }
            set { _mainDiagnostic = value; }
        }
        private List<Diagnostic> _secondaryDiagnostics;

        public List<Diagnostic> SecondaryDiagnostics
        {
            get { return _secondaryDiagnostics; }
            set { _secondaryDiagnostics = value; }
        }
        private Procedure _mainProcedure;

        public Procedure MainProcedure
        {
            get { return _mainProcedure; }
            set { _mainProcedure = value; }
        }
        private List<Procedure> _secondaryProcedures;

        public List<Procedure> SecondaryProcedures
        {
            get { return _secondaryProcedures; }
            set { _secondaryProcedures = value; }
        }
        private int _admissionTime;

        public int AdmissionTime
        {
            get { return _admissionTime; }
            set { _admissionTime = value; }
        }
        private byte _dischargeType;

        public byte DischargeType
        {
            get { return _dischargeType; }
            set { _dischargeType = value; }
        }

        private int? _ncc;
        public int? NCC
        {
            get { return _ncc; }
            set { _ncc = value; }
        }

        public Admission()
        {
            _secondaryDiagnostics = new List<Diagnostic>();
            _secondaryProcedures = new List<Procedure>();
        }

        public  List<Procedure> GetListOfProcedures()
        {
            List<Procedure> procedures = new List<Procedure>();
            if (SecondaryProcedures != null && SecondaryProcedures.Count != 0)
                procedures.AddRange(SecondaryProcedures);

            if (MainProcedure != null)
                procedures.Add(MainProcedure);
            return procedures;
        }

        public  bool HasHIV()
        {
            bool isHIVRelated = false;
            if (MainDiagnostic.IdSubClassDiagnostic == Constants.HIV_DIAGNOSTIC_SUBCLASS)
                isHIVRelated = true;
            foreach (Diagnostic d in SecondaryDiagnostics)
            {
                if (d.IdSubClassDiagnostic == Constants.HIV_DIAGNOSTIC_SUBCLASS)
                    isHIVRelated = true;
            }

            return isHIVRelated;
        }

        public bool HasMultipleTraumas()
        {
            int nrOfTraumas = 0;
            if (MainDiagnostic.Trauma == -1)
            {
                foreach (Diagnostic d in SecondaryDiagnostics)
                {
                    if (d.Trauma == 1)
                        nrOfTraumas++;
                }
                if (nrOfTraumas < 2)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        public  bool IsNewBorn()
        {
            if (YearAge == 0 && DaysAge != null)
            {
                if (DaysAge <= Constants.NEWBORN_DAYS)
                    return true;
                if (DaysAge > Constants.NEWBORN_DAYS && AdmissionWeight != null && AdmissionWeight <= Constants.NEWBORN_WEIGHT)
                    return true;
            }
            return false;
        }
        public  bool HasMalignantStatus()
        {
            if (MainDiagnostic.MalignantCondition == true)
                return true;
            if (SecondaryDiagnostics.Where(x => x.MalignantCondition == true).Count() > 0)
                return true;
            else
                return false;
        }

    }
}
