﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.Core
{
    public static class ApplicationSettings
    {
        private static String connectionString;
        public static String ConnectionString
        {
            get
            {
                if(string.IsNullOrEmpty(connectionString))
                    connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Decoder"].ConnectionString;
                return connectionString;
            }
        }
    }
}
