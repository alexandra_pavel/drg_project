﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.Core.Enums
{
    public enum PreCMDRuleEnum
    {
        TranslantFicat = 1,
        TransplantPlaman = 2,
        TransplantInima = 3,
        Traheoscopie = 5,
        TransplantAlogenic = 7,
        TransplantAutolog = 8,
        TransplantRenal = 11,
        Oxigenoterapie = 13,
        IntubatieVarsta16 = 14,
        Paraplegie = 17,
        HIV = 19,
        TraumatismMultiplu = 20,
        NouNascuti_Neonatal = 21
    }
}
