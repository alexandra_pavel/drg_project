﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.Core
{
    public enum Sex
    {
        Barbat = 1,
        Femeie = 2,
        Nedeterminat = 3,
        Necunoscut = 4
    }
}
