﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.Core
{
    public enum DRGCategoryTypes
    {
         [Description("S")]
        Surgical = 1,
         [Description("O")]
        Other = 2,
         [Description("M")]
        Medical = 3
    }

    public static class DRGCategoryTypesExtensions
    {
        public static string ToDescriptionString(this DRGCategoryTypes val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val.GetType().GetField(val.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }
}
