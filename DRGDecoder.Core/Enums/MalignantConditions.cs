﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.Core.Enums
{
    public enum MalignantConditions
    {
        WithMalignantState = 1,
        WithoutMalignantState = 2
    }
}
