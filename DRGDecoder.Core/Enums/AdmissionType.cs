﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.Core.Enums
{
    public enum AdmissionTypes
    {
        Continuous = 1,
        DayCare = 2
    }
}
