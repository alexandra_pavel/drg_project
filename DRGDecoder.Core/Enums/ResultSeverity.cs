﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.Core
{
    public enum ResultType
    {
        Error = 1,
        Warning = 2,
        Success= 3
    }
}
