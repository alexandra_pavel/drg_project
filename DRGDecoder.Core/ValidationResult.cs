﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.Core
{
    public class ValidationResult
    {
        public String Code { get; set; }
        public String Message { get; set;}
        public ResultType Severity {get; set;}
        public String Result
        {
            get { return this.Severity.ToString(); }
        }

        public List<ValidationResult> Items;

        public void ClearSuccesResults()
        {
            if (Items != null && Items.Count != 0)
            {
                List<ValidationResult> succes = new List<ValidationResult>();
                foreach (ValidationResult item in Items)
                {
                    if (item.Severity == ResultType.Success)
                        succes.Add(item);
                }

                foreach (ValidationResult item in succes)
                    Items.Remove(item);
            }
        }

        public ResultType GetValidationResult()
        {
            ResultType result = ResultType.Success;

            foreach(ValidationResult item in Items)
            {
                if (item.Severity == ResultType.Error)
                    return ResultType.Error;
                else
                    if (item.Severity == ResultType.Warning)
                        result = ResultType.Warning;
            }
            return result;
        }
    }
}
