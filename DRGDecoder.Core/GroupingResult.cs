﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.Core
{
    public class GroupingResult
    {
        public int CMD;
        public string GDRG;
        public string DRG;
        public double? RelativeValue;

        public bool isPreCMD;
        public bool isUnClassified;
    }
}
