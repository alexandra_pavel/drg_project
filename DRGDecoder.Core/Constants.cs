﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.Core
{
    public static class Constants
    {
        public static int UNCLASSIFIED_CMD = 24;
        public static string UNCLASSIFIED_GDRG = "9301";
        public static string UNCLASSIFIED_DRG = "93010";
        public static string TABLE_CODE_PARAPLAGIE_DP = "TAB-B60-1";
        public static string TABLE_CODE_PARAPLAGIE_DS = "TAB-B60-2";
        public static int HIV_DIAGNOSTIC_SUBCLASS = 43;
        public static int NEWBORN_DAYS = 28;
        public static int NEWBORN_WEIGHT = 2500;
        public static int NEWBORN_CMD = 15;

        public static int ABATERE_PROC_CHIRUGICALA_CMD = 24;
        public static string ABATERE_PROC_CHIRUGICALA_GDRG = "9101";
        public static string ABATERE_PROC_CHIRUGICALA_DRG = "91010";

        public static double NCC_ALPHA_PARAM = 0.4;
        public static int NCC_K_PARAM = 2;
    }
}
