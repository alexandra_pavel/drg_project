﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.Core.Exceptions
{
    public class IrelevantSurgicalProceduresException : Exception
    {
        public IrelevantSurgicalProceduresException()
        {
        }

        public IrelevantSurgicalProceduresException(string message)
            : base(message)
        {
        }

        public IrelevantSurgicalProceduresException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
