﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.Core.Exceptions
{
    public class UnclassifiedEpisodeException : Exception
    {
        public UnclassifiedEpisodeException()
        {
        }

        public UnclassifiedEpisodeException(string message)
            : base(message)
        {
        }

        public UnclassifiedEpisodeException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
