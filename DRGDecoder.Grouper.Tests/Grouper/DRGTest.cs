﻿using DRGDecoder.BLL;
using DRGDecoder.BLL.DataManagers;
using DRGDecoder.Core;
using DRGDecoder.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.Grouper.Tests.Grouper
{
    public class DRGTest
    {
        public GDRG TestWithMainDiagn1()
        {
            DiagnosticServiceRead diagnSrv = new DiagnosticServiceRead();
            Admission adm = new Admission()
            {
                AdmissionTime = 4,
                AdmissionWeight = 0,
                DaysAge = 0,
                DischargeType = 1,
                MainDiagnostic = diagnSrv.GetDiagnosticByCode("A02.1"),
                MainProcedure = null,
                Sex = 1,
                YearAge = 30
            };
            GrouperManager grupMan = new GrouperManager();
            GDRG drg = grupMan.GroupAdmissionToDRG(adm);
            return drg;
        }

        public GDRG TestWithMainDiagn2()
        {
            DiagnosticServiceRead diagnSrv = new DiagnosticServiceRead();
            ProcedureServiceRead procSrv = new ProcedureServiceRead();
            Admission adm = new Admission()
            {
                AdmissionTime = 4,
                AdmissionWeight = 0,
                DaysAge = 0,
                DischargeType = 1,
                MainDiagnostic = diagnSrv.GetDiagnosticByCode("S81.0"),
                MainProcedure = procSrv.GetProcedureByCode("90582-01"),
                Sex = 1,
                YearAge = 30
            };
            GrouperManager grupMan = new GrouperManager();
            GDRG drg = grupMan.GroupAdmissionToDRG(adm);
            return drg;
        }

        public GDRG TestWithMainDiagnAndHIVDS()
        {
            DiagnosticServiceRead diagnSrv = new DiagnosticServiceRead();
            ProcedureServiceRead procSrv = new ProcedureServiceRead();
            Admission adm = new Admission()
            {
                AdmissionTime = 4,
                AdmissionWeight = 0,
                DaysAge = 0,
                DischargeType = 1,
                MainDiagnostic = diagnSrv.GetDiagnosticByCode("A02.1"),
                MainProcedure = null,
                Sex = 1,
                YearAge = 30
            };
            adm.SecondaryDiagnostics.Add(diagnSrv.GetDiagnosticByCode("B24"));
            GrouperManager grupMan = new GrouperManager();
            GDRG drg = grupMan.GroupAdmissionToDRG(adm);
            return drg;
        }
    }
}
