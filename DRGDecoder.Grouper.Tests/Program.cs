﻿using DRGDecoder.Grouper.Tests.Grouper;
using DRGDecoder.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.Grouper.Tests
{
    class Program
    {
        static void Main(string[] args)
        {
            DRGTest tests = new DRGTest();
            GDRG result = tests.TestWithMainDiagn1();
            Console.WriteLine("CMD: {0}, GDRG: {1}, DRG: {2} ", result.CMDCode, result.CodeGDRG, result.CodeDRG);

            result = tests.TestWithMainDiagn2();
            Console.WriteLine("CMD: {0}, GDRG: {1}, DRG: {2} ", result.CMDCode, result.CodeGDRG, result.CodeDRG);

            result = tests.TestWithMainDiagnAndHIVDS();
            Console.WriteLine("CMD: {0}, GDRG: {1}, DRG: {2} ", result.CMDCode, result.CodeGDRG, result.CodeDRG);


        }
    }
}
