﻿using DRGDecoder.BLL.Utils;
using DRGDecoder.Core;
using DRGDecoder.Core.Enums;
using DRGDecoder.Core.Exceptions;
using DRGDecoder.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.BLL.DataManagers
{
    public class ProcessGDRGDivision: GenericDRGGrouperStep
    {
        Admission admission;
        public override GDRG ProcessDRGGrouping(Admission currentAdmission, List<GDRG> drgList)
        {
            //calculez NCC-ul : Calculez pentru fiecare Diagnostic NCD;
            string gdrg = drgList[0].CodeGDRG;
            admission = currentAdmission;
            if (!string.IsNullOrEmpty(gdrg))
            {

                double NCC = GetNCCForAdmission(gdrg);
                List<GDRG> finalDRG = RemoveDRGsByConstraints(NCC, drgList);
                if (finalDRG == null)
                    throw new UnclassifiedEpisodeException();

                else if (finalDRG.Count == 1)
                    return finalDRG.First();
                else
                    if (this.NextSequence != null)
                        return this.NextSequence.ProcessDRGGrouping(admission, finalDRG);
                    else
                        throw new UnclassifiedEpisodeException();
            }
            else
                throw new UnclassifiedEpisodeException();
        }

        private double GetNCCForAdmission(string gdrg)
        {
            ComputeNCDForEachDiagnostic(admission.SecondaryDiagnostics, gdrg, admission.Sex);
            List<Diagnostic> validDS = GrouperUtils.RemoveUnrrelevantDS(admission);
            double ncc;
            if (validDS.Count != 0)
                ncc = ComputeNCCBasedOnNCDs(validDS.OrderByDescending(x => x.NCD).ToList());
            else
                ncc = 0;

            return ncc;
        }

        private void ComputeNCDForEachDiagnostic(List<Diagnostic> list, string GDRG, int sexId)
        {
            DRGDecoder.BLL.Services.NCDServiceRead ncdSrv = new DRGDecoder.BLL.Services.NCDServiceRead();
            foreach (Diagnostic d in list)
            {
                d.NCD = ncdSrv.GetNCDByDiagnosticCodeAndGDRG(d.Code, GDRG, sexId);
            }
        }

        private int ComputeNCCBasedOnNCDs(List<Diagnostic> validDS)
        {
            double sum = 0;
            for (int i = 0; i < validDS.Count; i++)
                sum += validDS[i].NCD.Value * Math.Exp(-Constants.NCC_ALPHA_PARAM * (i + 1 - Constants.NCC_K_PARAM));

            double x = Math.Log(1 + sum) / (Math.Log(3 / Constants.NCC_ALPHA_PARAM) / 4);
            int roundedX = (int)Math.Round(x, 0);
            if (roundedX > 4)
                return 4;
            else
                return roundedX;
        }

        private List<GDRG> RemoveDRGsByConstraints(double ncc, List<GDRG> listDRG_3)
        {
            List<GDRG> removedDRGs = new List<GDRG>();
            foreach (GDRG drg in listDRG_3)
            {
                //check for admissionType criteria
                if (drg.AdmissionType == (int)AdmissionTypes.DayCare && admission.AdmissionTime != 1)
                    removedDRGs.Add(drg);
                else if (drg.AdmissionType == (int)AdmissionTypes.Continuous && admission.AdmissionTime == 1)
                    removedDRGs.Add(drg);

                //check for DischargeType criteria
                if (drg.HasDischargeTypeCondition && !drg.DischargeTypesAdmitted.Contains(admission.DischargeType.ToString()))
                    removedDRGs.Add(drg);

                //check for stare maligna criteria
                bool hasMalignantCriteria = admission.HasMalignantStatus();
                if (drg.IsForMalignantCondition != 0)
                {
                    if (drg.IsForMalignantCondition == (int)MalignantConditions.WithMalignantState && !hasMalignantCriteria)
                        removedDRGs.Add(drg);
                    else if (drg.IsForMalignantCondition == (int)MalignantConditions.WithoutMalignantState && hasMalignantCriteria)
                        removedDRGs.Add(drg);
                }

                //check for age criteria // DE REVENIT, PENTRU A LUA IN CALCUL SI SAU-urile cu CC
                if (drg.HasAgeCondition)
                {
                    int yearAge = admission.YearAge;
                    if (drg.AgeLowLimit.HasValue && drg.AgeHighLimit.HasValue
                        && (yearAge < drg.AgeLowLimit.Value || yearAge > drg.AgeHighLimit.Value))
                        removedDRGs.Add(drg);
                    else
                        if (drg.AgeHighLimit.HasValue && yearAge > drg.AgeHighLimit.Value)
                            removedDRGs.Add(drg);
                        else if (drg.AgeLowLimit.HasValue && yearAge < drg.AgeLowLimit.Value)
                            removedDRGs.Add(drg);
                }

                //check for NCC criteria
                if (drg.NCC_Condition)
                {
                    if (ncc < drg.NccLowLimit.Value || ncc > drg.NccHighLimit.Value)
                        removedDRGs.Add(drg);
                }
            }

                return listDRG_3.Except(removedDRGs).ToList();
        }
    }
}
