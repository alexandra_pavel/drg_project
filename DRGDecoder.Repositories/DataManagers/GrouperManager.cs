﻿using DRGDecoder.BLL.Interfaces;
using DRGDecoder.BLL.Services;
using DRGDecoder.Core;
using DRGDecoder.Core.Enums;
using DRGDecoder.Core.Exceptions;
using DRGDecoder.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.BLL.DataManagers
{
    public class GrouperManager
    {

        public GDRG GroupAdmissionToDRG(Admission CurrentAdmission)
        {
            IGDRGServiceRead gdrgSrvRead = new GDRGServiceRead();

            try
            {
                ProcessGDRGDivision proc4 = new ProcessGDRGDivision();
                ProcessProcedures proc3 = new ProcessProcedures();
                ProcessPreCMD proc2 = new ProcessPreCMD();
                ProcessCMD proc1 = new ProcessCMD();
                proc1.NextSequence = proc2;
                proc2.NextSequence = proc3;
                proc3.NextSequence = proc4;

                GDRG finalDRG= proc1.ProcessDRGGrouping(CurrentAdmission, null);
                if (finalDRG != null)
                    return finalDRG;
                else
                    throw new UnclassifiedEpisodeException(); 
            }
            catch (IrelevantSurgicalProceduresException ex)
            {
               return gdrgSrvRead.GetDRGByCode(Constants.ABATERE_PROC_CHIRUGICALA_DRG); 
            }
            catch(UnclassifiedEpisodeException ex)
            {
                return gdrgSrvRead.GetDRGByCode(Constants.UNCLASSIFIED_DRG);
            }
        }

    }
}
