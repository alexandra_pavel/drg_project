﻿using DRGDecoder.BLL.Services;
using DRGDecoder.BLL.Utils;
using DRGDecoder.Core;
using DRGDecoder.Core.Enums;
using DRGDecoder.Core.Exceptions;
using DRGDecoder.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.BLL.DataManagers
{
    public class ProcessPreCMD: GenericDRGGrouperStep
    {
        Admission admission;
        GDRGServiceRead gdrgSrvRead;

        public override GDRG ProcessDRGGrouping(Admission currentAdmission, List<GDRG> drgList)
        {
            admission = currentAdmission;
            gdrgSrvRead = new GDRGServiceRead();
            List<GDRG> newDRGList = ProccessPreCMDRules(drgList);
            if (newDRGList == null || newDRGList.Count == 0)
                throw new UnclassifiedEpisodeException();

            if (newDRGList.Count == 1)
                return newDRGList.First();
            else if (this.NextSequence != null)
                return this.NextSequence.ProcessDRGGrouping(admission, newDRGList);
            else
                throw new UnclassifiedEpisodeException();         
        }

        private List<GDRG> ProccessPreCMDRules(List<GDRG> drgList)
        {
            List<Procedure> procedures = admission.GetListOfProcedures();

            PreCMDRulesServiceRead preCMDSrvRead = new PreCMDRulesServiceRead();
            List<PreCMDRule> preCMDRules = preCMDSrvRead.GetPreCMDRules();
            List<GDRG> listDRG_2 = new List<GDRG>();

            foreach (PreCMDRule rule in preCMDRules)
            {
                switch ((PreCMDRuleEnum)rule.Id)
                {
                    case PreCMDRuleEnum.TranslantFicat:
                    case PreCMDRuleEnum.TransplantPlaman:
                    case PreCMDRuleEnum.TransplantInima:
                    case PreCMDRuleEnum.TransplantAlogenic:
                    case PreCMDRuleEnum.Oxigenoterapie:
                        bool isNewPreCMD = GrouperUtils.CheckForRegularPreCMD(procedures, rule.TableCode);
                        if (isNewPreCMD)
                        {
                            listDRG_2.Add(gdrgSrvRead.GetDRGByCode(rule.CodDRG));
                            return listDRG_2;
                        }
                        break;
                    case PreCMDRuleEnum.Traheoscopie:
                        bool isDRGA104 = GrouperUtils.CheckForRegularPreCMD(procedures, rule.TableCode);
                        if (isDRGA104 
                            && !(GrouperUtils.CheckForCMD(22, drgList) || (GrouperUtils.CheckForCMD(5, drgList) && admission.YearAge < 16)))
                        {
                            listDRG_2.AddRange(gdrgSrvRead.GetDRGsForGroup(rule.CodeGDRG));
                            return listDRG_2;
                        }
                        break;
                    case PreCMDRuleEnum.IntubatieVarsta16:
                        bool isDRGA202 = GrouperUtils.CheckForRegularPreCMD(procedures, rule.TableCode);
                        if (isDRGA202 && admission.YearAge < 16 
                            && (!GrouperUtils.CheckForCMD(22, drgList) && !GrouperUtils.CheckForCMD(5, drgList)))
                        {
                            listDRG_2.AddRange(gdrgSrvRead.GetDRGsForGroup(rule.CodeGDRG));
                            return listDRG_2;
                        }
                        break;
                    case PreCMDRuleEnum.TransplantAutolog:
                    case PreCMDRuleEnum.TransplantRenal:
                        isNewPreCMD = GrouperUtils.CheckForRegularPreCMD(procedures, rule.TableCode);
                        if (isNewPreCMD)
                        {
                            listDRG_2.AddRange(gdrgSrvRead.GetDRGsForGroup(rule.CodeGDRG));
                            return listDRG_2;
                        }
                        break;
                    case PreCMDRuleEnum.Paraplegie:
                        isNewPreCMD = GrouperUtils.CheckDiagnosticsForPreCMD(admission, rule.TableCode);
                        if (isNewPreCMD)
                        {
                            listDRG_2.AddRange(gdrgSrvRead.GetDRGsForGroup(rule.CodeGDRG));
                            return listDRG_2;
                        }
                        break;
                    case PreCMDRuleEnum.HIV:
                        bool isHIVRelated = admission.HasHIV();
                        if (isHIVRelated)
                        {
                            listDRG_2 = drgList.Where(x => x.CodeDRG.StartsWith("S")).ToList();
                            drgList = listDRG_2;
                        }
                        else
                        {
                            listDRG_2 = drgList.Where(x => x.CodeDRG.StartsWith("S") == false).ToList();
                            drgList = listDRG_2;
                        }
                        break;
                    case PreCMDRuleEnum.TraumatismMultiplu:
                        if (admission.HasMultipleTraumas())
                            listDRG_2 = drgList.Where(x => x.CodeDRG.StartsWith("W")).ToList();
                        else
                        {
                            listDRG_2 = drgList.Where(x => x.CodeDRG.StartsWith("W") == false).ToList();
                            drgList = listDRG_2;
                        }
                        break;
                    case PreCMDRuleEnum.NouNascuti_Neonatal:
                        if (admission.IsNewBorn())
                        {
                            listDRG_2 = drgList.Where(x => x.CMDCode == Constants.NEWBORN_CMD).ToList();
                        }
                        else
                        {
                            listDRG_2 = drgList.Where(x => x.CMDCode != Constants.NEWBORN_CMD).ToList();
                            drgList = listDRG_2;
                        }
                        break;
                }
            }
            return listDRG_2;
        }
    }
}
