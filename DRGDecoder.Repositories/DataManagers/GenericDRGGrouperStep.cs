﻿using DRGDecoder.Core;
using DRGDecoder.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.BLL.DataManagers
{
    public abstract class GenericDRGGrouperStep
    {
        private GenericDRGGrouperStep nextSequence = null;
        public GenericDRGGrouperStep NextSequence
        {
            set { this.nextSequence = value; }
            get { return this.nextSequence; }
        }

        public abstract GDRG ProcessDRGGrouping(Admission currentAdmission, List<GDRG> drgList);

    }
}
