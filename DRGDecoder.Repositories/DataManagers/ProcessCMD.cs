﻿using DRGDecoder.BLL.Services;
using DRGDecoder.Core;
using DRGDecoder.Core.Exceptions;
using DRGDecoder.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.BLL.DataManagers
{
    public class ProcessCMD : GenericDRGGrouperStep
    {
        private GDRGServiceRead gdrgSrvRead = new GDRGServiceRead();

        public override GDRG ProcessDRGGrouping(Admission currentAdmission, List<GDRG> drgList)
        {
            drgList = gdrgSrvRead.GetDRGsForDiagnostic(currentAdmission.MainDiagnostic.Code);
            if (drgList == null || drgList.Count == 0)
                throw new UnclassifiedEpisodeException();
            else
                if (drgList != null && drgList.Count == 1
                    && currentAdmission.MainProcedure == null
                    && (currentAdmission.SecondaryDiagnostics == null || currentAdmission.SecondaryDiagnostics.Count == 0))
                    return drgList.First();
                else if (NextSequence != null)
                    return this.NextSequence.ProcessDRGGrouping(currentAdmission, drgList);
                else
                    throw new UnclassifiedEpisodeException();
        }
    }
}
