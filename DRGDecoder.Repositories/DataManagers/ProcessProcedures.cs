﻿using DRGDecoder.BLL.Utils;
using DRGDecoder.Core;
using DRGDecoder.Core.Exceptions;
using DRGDecoder.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.BLL.DataManagers
{
    public class ProcessProcedures: GenericDRGGrouperStep
    {
        public override GDRG ProcessDRGGrouping(Admission currentAdmission, List<GDRG> drgList)
        {
            List<GDRG> newDRGList = ProcessAdmissionProcedures(currentAdmission, drgList);
            if (newDRGList == null || newDRGList.Count == 0)
                throw new UnclassifiedEpisodeException();
            else
                if (this.NextSequence != null)
                    return this.NextSequence.ProcessDRGGrouping(currentAdmission, newDRGList);
                else
                    throw new UnclassifiedEpisodeException();
            
        }

        private List<GDRG> ProcessAdmissionProcedures(Admission CurrentAdmission, List<GDRG> listDRG_1)
        {
            List<Procedure> procedures = CurrentAdmission.GetListOfProcedures();
            //no procedures
            if (procedures == null || procedures.Count == 0)
                return GrouperUtils.RemoveSurgicalAndOtherDRGs(listDRG_1);

            //with surgical procedures
            else if (procedures.Where(x => x.IsNotDoneInOR == false).Count() != 0)
            {
                List<GDRG> listDRG_3 = GrouperUtils.ProcessAdmissionForSurgicalProcedures(procedures.Where(x => x.IsNotDoneInOR == false).ToList(), listDRG_1);
                    if (listDRG_3 != null && listDRG_3.Count != 0)
                        return listDRG_3;
                    else
                        throw new IrelevantSurgicalProceduresException();
            }
            //with medical procedures
            else
            {
                List<GDRG> listDRG_3 = GrouperUtils.ProcessAdmissionForSurgicalProcedures(procedures.Where(x => x.IsNotDoneInOR == true).ToList(), listDRG_1);
                if (listDRG_3 != null && listDRG_3.Count != 0)
                    return listDRG_3;
                else
                    return GrouperUtils.RemoveSurgicalAndOtherDRGs(listDRG_1);

            }
        }
    }
}
