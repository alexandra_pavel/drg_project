﻿using DRGDecoder.Core;
using DRGDecoder.Model;
using DRGDecoder.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DRGDecoder.BLL.Services;

namespace DRGDecoder.BLL.DataManagers
{
    public class ValidationManager
    {
        private Admission admission;
        private IValidationRulesServiceRead validationRuleService = new ValidationRulesServiceRead();
        private ISexConflictTypeServiceRead sexConflictService = new SexConflictTypeServiceRead();
        private IAgeConflictServiceRead ageConflictService = new AgeConflictServiceRead();

        public ValidationResult ClinicalValidation(Admission admission)
        {
            this.admission = admission;
            ValidationResult result = new ValidationResult();
            List<ValidationRule> rules = validationRuleService.GetClinicalValidationRules();
            if(rules != null)
                result = ValidateAdmission(rules);
            return result;
        }

        private ValidationResult ValidateAdmission(List<ValidationRule> rules)
        {
            ValidationResult result = new ValidationResult();
            result.Items = new List<ValidationResult>();

            foreach (ValidationRule rule in rules)
            { 
                switch (rule.DisplayName)
                {
                    case "DiagnosticSexConflict":
                        result.Items.AddRange(ValidateDiagnosticsForSexConflict(rule));
                        break;
                    case "DiagnosticAgeConflict":
                        result.Items.AddRange(ValidateDiagnosticsForAgeConflict(rule));
                        break;
                    case "MainDiagnosticConflict":
                        result.Items.Add(ValidateMainDiagnostic(rule));
                        break;
                    case "ProcedureSexConflict":
                        result.Items.AddRange(ValidateProceduresForSexConflict(rule));
                        break;
                    case "ProcedureAgeConflict":
                        result.Items.AddRange(ValidateProceduresForAgeConflict(rule));
                        break;
                }
            }
            return result;
        }

        private List<ValidationResult> ValidateProceduresForSexConflict(ValidationRule rule)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            results.Add(ValidateProcedureForSexConflict(rule, admission.MainProcedure));

            foreach (Procedure procedure in admission.SecondaryProcedures)
            {
                results.Add(ValidateProcedureForSexConflict(rule, procedure));
            }

            return results;
        }

        private ValidationResult ValidateProcedureForSexConflict(ValidationRule rule, Procedure procedure)
        {
            ValidationResult result = new ValidationResult();
            result.Severity = ResultType.Success;

            if(rule !=null && procedure != null)
            {
                if (procedure.IdSexConflictType != null && procedure.IdSexConflictType != 0)
                {
                    SexConflictType conflictType = sexConflictService.getSexConflictTypeById(procedure.IdSexConflictType);
                    if (conflictType != null && conflictType.SexCode == admission.Sex)
                    {
                        if (conflictType.IsFatal == 1)
                            result.Severity = ResultType.Error;
                        else
                            result.Severity = ResultType.Warning;

                        result.Message = String.Format(rule.Description, procedure.Code);
                    }
                }
            }
            return result;
        }

        private List<ValidationResult> ValidateDiagnosticsForAgeConflict(ValidationRule rule)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            results.Add(ValidateDiagnosticForAgeConflict(rule, admission.MainDiagnostic));
            foreach (Diagnostic diagnostic in admission.SecondaryDiagnostics)
            {
                results.Add(ValidateDiagnosticForAgeConflict(rule, diagnostic));
            }
            return results;
        }

        private ValidationResult ValidateDiagnosticForAgeConflict(ValidationRule rule, Diagnostic diagnostic)
        { 
            ValidationResult result = new ValidationResult();
            result.Severity = ResultType.Success;

            if (rule != null && diagnostic != null)
            { 
                if(diagnostic.IdAgeConflictType !=null && diagnostic.IdAgeConflictType != 0)
                {
                    AgeConflictType conflictType = ageConflictService.GetAgeConflictById(diagnostic.IdAgeConflictType.Value);
                    if (conflictType != null)
                    {
                        int age = admission.YearAge == 0 ? admission.DaysAge.Value : admission.YearAge;
                        if (!(age >= conflictType.MinAge && age <= conflictType.MaxAge))
                        {
                            result.Message = String.Format(rule.Description, diagnostic.Code);
                            if (conflictType.isFatal == 1)
                                result.Severity = ResultType.Error;
                            else
                                result.Severity = ResultType.Warning;                            
                        }
                    }
                }
            }
            return result;
        }

        private List<ValidationResult> ValidateProceduresForAgeConflict(ValidationRule rule)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            results.Add(ValidateProcedureForAgeConflict(rule,admission.MainProcedure));
            foreach (Procedure procedure in admission.SecondaryProcedures)
            {
                results.Add(ValidateProcedureForAgeConflict(rule, procedure));
            }

            return results;
        }

        private ValidationResult ValidateProcedureForAgeConflict(ValidationRule rule, Procedure procedure)
        {
            ValidationResult result = new ValidationResult();
            result.Severity = ResultType.Success;

            if (procedure != null && rule != null)
            {
                if (procedure.IdAgeConflictType != null && procedure.IdAgeConflictType != 0)
                {
                    AgeConflictType conflictType = ageConflictService.GetAgeConflictById(procedure.IdAgeConflictType.Value);
                    if (conflictType != null)
                    {
                        int age = admission.YearAge == 0 ? admission.DaysAge.Value : admission.YearAge;
                        if (!(age >= conflictType.MinAge && age <= conflictType.MaxAge))
                        {
                            result.Message = String.Format(rule.Description, procedure.Code);
                            if (conflictType.isFatal == 1)
                                result.Severity = ResultType.Error;
                            else
                                result.Severity = ResultType.Warning;
                        }
                    }
                }
            }
            return result;
        }

        private List<ValidationResult> ValidateDiagnosticsForSexConflict(ValidationRule rule)
        {
            List<ValidationResult> results = new List<ValidationResult>();
            results.Add(ValidateDiagnosticForSexConflict(rule, admission.MainDiagnostic, admission.Sex));

            foreach (Diagnostic secDiagn in admission.SecondaryDiagnostics)
            {
                results.Add(ValidateDiagnosticForSexConflict(rule, secDiagn, admission.Sex));
            }
            return results;
        }

        private ValidationResult ValidateDiagnosticForSexConflict(ValidationRule rule, Diagnostic diagnostic, int idSex)
        {
            ValidationResult result = new ValidationResult();
            result.Severity = ResultType.Success;

            if (diagnostic.IdSexConflictType.HasValue && diagnostic.IdSexConflictType != 0)
            {
                SexConflictType type = sexConflictService.getSexConflictTypeById(diagnostic.IdSexConflictType);
                if (type != null)
                {
                    if (type.SexCode == idSex)
                    {
                        result.Message = String.Format(rule.Description,diagnostic.Code);
                        if (type.IsFatal == 1)
                            result.Severity = ResultType.Error;
                        else
                            result.Severity = ResultType.Warning;
                    }
                }
            }
            return result;
        }

        private ValidationResult ValidateMainDiagnostic(ValidationRule rule)
        {
            ValidationResult result = new ValidationResult();
            result.Severity = ResultType.Success;

            if(admission.MainDiagnostic.UnacceptableAsPrimary == 1)
            {
                result.Severity = ResultType.Error;
                result.Message = rule.Description;
            }

            return result;
        }
    }
}
