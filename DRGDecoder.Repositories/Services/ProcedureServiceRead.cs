﻿using DRGDecoder.Model;
using DRGDecoder.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.BLL
{
    public class ProcedureServiceRead : IProcedureServiceRead
    {
        public List<Procedure> GetProceduresByFilter(string filter)
        {
            List<Procedure> procedureList = null;
            if (!string.IsNullOrEmpty(filter))
            {
                using (GlobalContext context = new GlobalContext())
                {
                    procedureList = context.Procedures
                        .Where(p => p.Code.StartsWith(filter) || p.DisplayName.Contains(filter))
                        .ToList();
                }
            }
            return procedureList;
        }

        public Procedure GetProcedureByCode(string code)
        {
            if (!string.IsNullOrEmpty(code))
            {
                using (GlobalContext context = new GlobalContext())
                {
                    Procedure procedure = context.Procedures
                        .Where(p => p.Code == code)
                        .FirstOrDefault();
                    return procedure;
                }
            }
            return null;
        }

        public Procedure GetProcedureByName(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                using (GlobalContext context = new GlobalContext())
                {
                    Procedure procedure = context.Procedures
                        .Where(p => p.DisplayName == name)
                        .FirstOrDefault();
                    return procedure;
                }
            }
            return null;
        }
    }
}
