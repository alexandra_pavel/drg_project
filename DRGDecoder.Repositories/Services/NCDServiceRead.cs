﻿using DRGDecoder.BLL.Interfaces;
using DRGDecoder.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.BLL.Services
{
    public class NCDServiceRead : INCDServiceRead
    {
        public int GetNCDByDiagnosticCodeAndGDRG(string diagnCode, string GdrgCode, int sexId)
        {
            int? ncd;
            using(GlobalContext context= new GlobalContext())
            {
                ncd = context.NCCDiagnostics
                    .Where(x => x.GDRG.Contains(GdrgCode)
                        && (x.SexConstraint == 0 || x.SexConstraint == sexId)
                        && x.CodDiagn == diagnCode)
                    .Select(x => x.NCD)
                    .FirstOrDefault();
            }
            if (ncd.HasValue)
                return ncd.Value;
            else
                return 0;
        }
    }
}
