﻿using DRGDecoder.Core;
using DRGDecoder.Model;
using DRGDecoder.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.BLL
{
    public class DiagnosticServiceRead : IDiagnosticServiceRead
    {
        public List<Diagnostic> GetDiagnosticsByFilter(string filter)
        {
            List<Diagnostic> diagnosticList = null;

            if (!string.IsNullOrEmpty(filter))
            {
                using (GlobalContext context = new GlobalContext())
                {
                    diagnosticList = context.Diagnostics
                        .Where(d => d.Code.StartsWith(filter) || d.DisplayName.Contains(filter))
                        .Take(20)
                        .ToList();
                }
            }
            return diagnosticList;
        }

        public Diagnostic GetDiagnosticByCode(string code)
        {
            if (!string.IsNullOrEmpty(code))
            {
                using (GlobalContext context = new GlobalContext())
                {
                    Diagnostic diagnostic = context.Diagnostics.Where(d => d.Code == code).FirstOrDefault();
                    return diagnostic;
                }
            }
            return null;
        }

        public Diagnostic GetDiagnosticByName(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                using (GlobalContext context = new GlobalContext())
                {
                    Diagnostic diagnostic = context.Diagnostics.Where(d => d.DisplayName == name).FirstOrDefault();
                    return diagnostic;
                }
            }
            return null;
        }
        public List<Diagnostic> GetMainDiagnosticsByFilter(string filter)
        {
            List<Diagnostic> diagnosticList = null;

            if (!string.IsNullOrEmpty(filter))
            {
                using (GlobalContext context = new GlobalContext())
                {
                    diagnosticList = context.Diagnostics
                        .Where(d => d.Code.StartsWith(filter) || d.DisplayName.Contains(filter) && d.UnacceptableAsPrimary == 0)
                        .Take(20)
                        .ToList();
                }
            }
            return diagnosticList;
        }
    }
}
