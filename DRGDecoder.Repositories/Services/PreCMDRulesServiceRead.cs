﻿using DRGDecoder.BLL.Interfaces;
using DRGDecoder.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.BLL.Services
{
    public class PreCMDRulesServiceRead : IPreCMDRulesServiceRead
    {

        public List<PreCMDRule> GetPreCMDRules()
        {
            List<PreCMDRule> rules = new List<PreCMDRule>();
            using(GlobalContext context = new GlobalContext())
            {
                rules = context.PreCMDRules.ToList();
            }
            return rules;
        }
    }
}
