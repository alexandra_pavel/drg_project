﻿using DRGDecoder.Model;
using DRGDecoder.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.BLL
{
    public class SexConflictTypeServiceRead: ISexConflictTypeServiceRead
    {
        public SexConflictType getSexConflictTypeById(int? id)
        {
            SexConflictType type = null;
            if(id.HasValue)
            {
                using (GlobalContext context = new GlobalContext())
                {
                    type = context.SexConflictTypes.Where(x => x.Id == id).FirstOrDefault();
                }
            }
            return type;
        }
    }
}
