﻿using DRGDecoder.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.BLL
{
    public class DischargeTypeServiceRead : IDischargeTypeServiceRead
    {
        public List<DischargeType> GetAllDischargeTypes()
        {
            List<DischargeType> dischargeTypes;
            using (GlobalContext context = new GlobalContext())
            {
                dischargeTypes = context.DischargeTypes
                    .ToList();
            }
            return dischargeTypes;
        }
    }
}
