﻿using DRGDecoder.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DRGDecoder.Model;

namespace DRGDecoder.BLL.Services
{
    public class ValidationRulesServiceRead : IValidationRulesServiceRead
    {
        public List<ValidationRule> GetClinicalValidationRules()
        {
            List<ValidationRule> rules = null;
            using (GlobalContext context = new GlobalContext())
            {
                rules = context.ValidationRules.Where(x => x.Area == 1).ToList();
            }

            return rules;
        }
    }
}
