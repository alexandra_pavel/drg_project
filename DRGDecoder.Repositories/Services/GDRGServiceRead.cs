﻿using DRGDecoder.BLL.Interfaces;
using DRGDecoder.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.BLL.Services
{
    public class GDRGServiceRead: IGDRGServiceRead
    {
        public int GetCMDForGDRG(string GDRG_code)
        {
           
           using(GlobalContext context = new GlobalContext())
           {
               int cmd = context.GDRGs.Where(x => x.CodeGDRG == GDRG_code).First().CMDCode;
               return cmd;
           }
        }

        public List<GDRG> GetDRGsForDiagnostic(string diagnCode)
        {
            List<GDRG> drgs = new List<GDRG>();
            using (GlobalContext context = new GlobalContext())
            {
                drgs = context.DiagDRG_Associations
                       .Include("GDRG")
                       .Where(x => x.ICDX_Code == diagnCode)
                       .Select(x => x.GDRG)
                       .ToList();
            }
            return drgs;
        }


        public GDRG GetDRGByCode(string drgCode)
        {
            using(GlobalContext context = new GlobalContext())
            {
                return context.GDRGs.Where(x => x.CodeDRG == drgCode).First();
            }
        }


        public List<GDRG> GetDRGsForGroup(string gdrg_Code)
        {
            using(GlobalContext context = new GlobalContext())
            {
                return context.GDRGs.Where(x => x.CodeGDRG == gdrg_Code).ToList();
            }
        }


        public List<GDRG> GetAdiacentDRGsToProcedure(string proc_Code)
        {
            using(GlobalContext context = new GlobalContext())
            {
                return context.ProcDRG_Associations
                    .Include("GDRG")
                    .Where(x => x.CodProc == proc_Code)
                    .Select(x => x.GDRG)
                    .ToList();
            }
        }
    }
}
