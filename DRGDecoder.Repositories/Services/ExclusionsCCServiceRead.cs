﻿using DRGDecoder.BLL.Interfaces;
using DRGDecoder.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.BLL.Services
{
    public class ExclusionsCCServiceRead : IExclusionsCCServiceRead
    {
        public bool IsSecondaryDiagnosticExcludedAsCC(string SDCode, string PDCode)
        {
            bool isExcluded = false;
            using(GlobalContext context = new GlobalContext())
            {
                ExclusionsCC cc = context.ExclusionsCCs
                    .Where(x => x.Cod_Diagn == SDCode && x.DiagnPrincAnulatoare.Contains(PDCode))
                    .FirstOrDefault();
                if (cc != null)
                    isExcluded = true;
            }
            return isExcluded;
        }
    }
}
