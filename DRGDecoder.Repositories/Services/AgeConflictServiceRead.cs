﻿using DRGDecoder.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DRGDecoder.Model;

namespace DRGDecoder.BLL.Services
{
    public class AgeConflictServiceRead : IAgeConflictServiceRead
    {
        public AgeConflictType GetAgeConflictById(int id)
        {
            AgeConflictType conflict = new AgeConflictType();
            using (GlobalContext context = new GlobalContext())
            {
                conflict = context.AgeConflictTypes.Where(x => x.Id == id).First();
            }

            return conflict;
        }
    }
}
