﻿using DRGDecoder.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.BLL.Interfaces
{
    public interface IProcedureServiceRead
    {
        List<Procedure> GetProceduresByFilter(string filter);
        Procedure GetProcedureByCode(string code);
        Procedure GetProcedureByName(string code);
    }
}
