﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.BLL.Interfaces
{
    public interface IExclusionsCCServiceRead
    {
        bool IsSecondaryDiagnosticExcludedAsCC(string SDCode, string PDCode);
    }
}
