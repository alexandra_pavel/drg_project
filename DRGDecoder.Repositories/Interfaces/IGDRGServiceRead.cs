﻿using DRGDecoder.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.BLL.Interfaces
{
    public interface IGDRGServiceRead
    {
        List<GDRG> GetDRGsForDiagnostic(string diagnCode);
        int GetCMDForGDRG(string GDRG_code);
        GDRG GetDRGByCode(string drgCode);
        List<GDRG> GetDRGsForGroup(string gdrg_Code);
        List<GDRG> GetAdiacentDRGsToProcedure(string proc_Code);
    }
}
