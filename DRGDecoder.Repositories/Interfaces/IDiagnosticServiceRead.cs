﻿using DRGDecoder.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.BLL.Interfaces
{
    public interface IDiagnosticServiceRead
    {
        List<Diagnostic> GetDiagnosticsByFilter(string filter);
        Diagnostic GetDiagnosticByCode(string code);
        Diagnostic GetDiagnosticByName(string name);

        List<Diagnostic> GetMainDiagnosticsByFilter(string filter);
    }
}
