﻿using DRGDecoder.BLL.Services;
using DRGDecoder.Core;
using DRGDecoder.Core.Exceptions;
using DRGDecoder.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DRGDecoder.BLL.Utils
{
   public static class GrouperUtils
    {
       public static bool CheckForRegularPreCMD(List<Procedure> procedures, string tableCode)
       {
           bool isPreCMD = false;
           foreach (Procedure proc in procedures)
           {
               if (string.Compare(proc.TableCode, tableCode) == 0)
                   isPreCMD = true;
           }
           return isPreCMD;
       }

       public static bool CheckForCMD(int cmd, List<GDRG> list)
       {
           bool containsCMD = false;
           foreach (GDRG gdrg in list)
           {
               if (gdrg.CMDCode == cmd)
                   containsCMD = true;
           }
           return containsCMD;
       }

       public static bool CheckDiagnosticsForPreCMD(Admission CurrentAdmission, string tableCode)
       {
           if (CurrentAdmission.MainDiagnostic.TableCode != null
               && CurrentAdmission.MainDiagnostic.TableCode.Contains(Constants.TABLE_CODE_PARAPLAGIE_DP))
               return true;
           foreach (Diagnostic ds in CurrentAdmission.SecondaryDiagnostics)
           {
               if (ds.TableCode != null && ds.TableCode.Contains(Constants.TABLE_CODE_PARAPLAGIE_DS))
                   return true;
           }

           return false;
       }

       public static List<GDRG> ProcessAdmissionForSurgicalProcedures(List<Procedure> procedures, List<GDRG> listDRG_1)
       {
           List<GDRG> proceduresDRGs = GetAdiacentDRGSToListOfProcedures(procedures);
           int drgCMD = listDRG_1.Select(x => x.CMDCode).First();

           List<GDRG> intersectDRGs = proceduresDRGs.Where(x => x.CMDCode == drgCMD).ToList();
           if (intersectDRGs == null || intersectDRGs.Count == 0)
               throw new IrelevantSurgicalProceduresException();

           else if (intersectDRGs.Select(x => x.CodeGDRG).Distinct().Count() > 1)
           {
               string firstGDRG = intersectDRGs.AsQueryable()
                   .Select(x => x.CodeGDRG)
                   .First();
               return intersectDRGs.Where(x => string.Compare(x.CodeGDRG, firstGDRG) == 0).ToList();
           }
           else
               return intersectDRGs;
       }

       public static List<GDRG> GetAdiacentDRGSToListOfProcedures(List<Procedure> procedures)
       {
           GDRGServiceRead gdrgSrvRead = new GDRGServiceRead();
           List<GDRG> list = new List<GDRG>();
           foreach (Procedure p in procedures)
           {
               list.AddRange(gdrgSrvRead.GetAdiacentDRGsToProcedure(p.Code2011));
           }

           return list;
       }

       public static List<GDRG> RemoveSurgicalAndOtherDRGs(List<GDRG> listDRG_1)
       {
           List<GDRG> surgicalAndOther = listDRG_1
               .Where(x => string.Compare(x.CategoryType, DRGCategoryTypes.Surgical.ToDescriptionString()) == 0 ||
               string.Compare(x.CategoryType, DRGCategoryTypes.Other.ToDescriptionString()) == 0).ToList();
           if (surgicalAndOther != null && surgicalAndOther.Count != 0)
               return listDRG_1.Except(surgicalAndOther).ToList();
           else
               return listDRG_1;
       }

       public static List<Diagnostic> RemoveUnrrelevantDS(Admission CurrentAdmission)
       {
           ExclusionsCCServiceRead exclusionsSrv = new ExclusionsCCServiceRead();

           List<Diagnostic> removedDS = new List<Diagnostic>();

           foreach (Diagnostic d in CurrentAdmission.SecondaryDiagnostics)
           {
               if (exclusionsSrv.IsSecondaryDiagnosticExcludedAsCC(d.Code, CurrentAdmission.MainDiagnostic.Code))
                   removedDS.Add(d);
           }

           if (removedDS.Count != 0)
               return CurrentAdmission.SecondaryDiagnostics.Except(removedDS).ToList();
           else
               return CurrentAdmission.SecondaryDiagnostics;

       }

    }
}
