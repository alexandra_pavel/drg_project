﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DRGDecoder.UI.Startup))]
namespace DRGDecoder.UI
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
