﻿using DRGDecoder.Core;
using DRGDecoder.Model;
using DRGDecoder.BLL;
using DRGDecoder.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using DRGDecoder.BLL.DataManagers;
using System.Text;
using System.Drawing;
using DRGDecoder.BLL.Services;

namespace DRGDecoder.UI
{
    public partial class PatientICM : System.Web.UI.Page
    {
        private IDiagnosticServiceRead _diagnosticManager;
        public IDiagnosticServiceRead DiagnosticManager{
            get
            {
                if(_diagnosticManager == null)
                {
                    _diagnosticManager = new DiagnosticServiceRead();
                }
                return _diagnosticManager;
            }
        }

        private IProcedureServiceRead _procedureManager;
        public IProcedureServiceRead ProcedureManager
        {
            get 
            {
                if(_procedureManager == null)
                {
                    _procedureManager = new ProcedureServiceRead();
                }
                return _procedureManager;
            }
        }

        public Admission CurrentAdmission
        {
            get
            {
                if (Session["CurrentAdmission"] == null)
                    Session["CurrentAdmission"] = new Admission();
                return (Admission)Session["CurrentAdmission"];
            }
            set
            {
                Session["CurrentAdmission"] = value; 
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                rcbGendre.DataSource = Enum.GetValues(typeof(Sex));
                rcbGendre.DataBind();

                DischargeTypeServiceRead dtRead = new DischargeTypeServiceRead();
                rcbDischargeType.DataSource = dtRead.GetAllDischargeTypes();
                rcbDischargeType.DataBind();

                if (lvErrors.Items.Count == 0)
                    lvErrors.Visible = false;
            }
        }

        protected void RcbDiagnostics_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox combo = sender as RadComboBox;
            rcbDiagnostics.ToolTip = combo.SelectedValue;
        }

        protected void RcbProcedure_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox combo = sender as RadComboBox;
            rcbProcedure.ToolTip = combo.SelectedValue;
        }

        protected void RgSecondaryDiagn_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            rgSecondaryDiagn.DataSource = CurrentAdmission.SecondaryDiagnostics;
        }

        protected void GridDiagnosticsCombo_TextChanged(object sender, EventArgs e)
        {
            RadComboBox combo = (RadComboBox)sender;
            string filter = combo.Text;
            if (!string.IsNullOrEmpty(filter))
            {
                combo.Items.Clear();
                combo.DataSource = DiagnosticManager.GetDiagnosticsByFilter(filter);
                combo.DataBind();
            }
        }

        protected void RgSecondaryDiagn_InsertCommand(object sender, GridCommandEventArgs e)
        {
            GridItem item = e.Item;
            RadComboBox combo = (RadComboBox)item.FindControl("gridDiagnosticsCombo");
            Diagnostic diagnToAdd = DiagnosticManager.GetDiagnosticByCode(combo.SelectedValue);
            if (diagnToAdd != null)
            {
                CurrentAdmission.SecondaryDiagnostics.Add(diagnToAdd);
            }
        }

        protected void RgSecondaryDiagn_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            if (e.Item is GridEditableItem)
            {
                GridEditableItem item = (GridEditableItem)e.Item;
                int id = Convert.ToInt32(item.GetDataKeyValue("Id").ToString());
                RadComboBox combo = (RadComboBox)item.FindControl("gridDiagnosticsCombo");
                Diagnostic oldDiagn = CurrentAdmission.SecondaryDiagnostics.Where(x => x.Id == id).First();
                CurrentAdmission.SecondaryDiagnostics.Remove(oldDiagn);

                Diagnostic newDiagn = DiagnosticManager.GetDiagnosticByCode(combo.SelectedValue);
                if (newDiagn != null)
                {
                    CurrentAdmission.SecondaryDiagnostics.Add(newDiagn);
                }
            }
        }

        protected void RgSecondaryDiagn_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            GridEditableItem item = (GridEditableItem)e.Item;
            int id = Convert.ToInt32(item.GetDataKeyValue("Id").ToString());
            Diagnostic oldDiagn = CurrentAdmission.SecondaryDiagnostics.Where(x => x.Id == id).First();
            CurrentAdmission.SecondaryDiagnostics.Remove(oldDiagn);
        }

        protected void gridDiagnosticsCombo_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
        {
            RadComboBox combo = (RadComboBox)sender;
            string filter = e.Text;
            if (!string.IsNullOrEmpty(filter))
            {
                combo.Items.Clear();
                combo.DataSource = DiagnosticManager.GetDiagnosticsByFilter(filter);
                combo.DataBind();
            }
        }

        protected void RgSecondaryProc_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            RgSecondaryProc.DataSource = CurrentAdmission.SecondaryProcedures;
        }

        protected void RgSecondaryProc_InsertCommand(object sender, GridCommandEventArgs e)
        {
            GridItem item = e.Item;
            RadComboBox combo = (RadComboBox)item.FindControl("gridProceduresCombo");
            Procedure procToAdd = ProcedureManager.GetProcedureByCode(combo.SelectedValue);
            if (procToAdd != null)
            {
                CurrentAdmission.SecondaryProcedures.Add(procToAdd);
            }
        }

        protected void RgSecondaryProc_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            if (e.Item is GridEditableItem)
            {
                GridEditableItem item = (GridEditableItem)e.Item;
                int id = Convert.ToInt32(item.GetDataKeyValue("Id").ToString());
                RadComboBox combo = (RadComboBox)item.FindControl("gridProceduresCombo");
                Procedure oldProc = CurrentAdmission.SecondaryProcedures.Where(x => x.Id == id).First();
                CurrentAdmission.SecondaryProcedures.Remove(oldProc);

                Procedure newProc = ProcedureManager.GetProcedureByCode(combo.SelectedValue);
                if (newProc != null)
                {
                    CurrentAdmission.SecondaryProcedures.Add(newProc);
                }
            }
        }

        protected void RgSecondaryProc_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            GridEditableItem item = (GridEditableItem)e.Item;
            int id = Convert.ToInt32(item.GetDataKeyValue("Id").ToString());
            Procedure oldProc = CurrentAdmission.SecondaryProcedures.Where(x => x.Id == id).First();
            CurrentAdmission.SecondaryProcedures.Remove(oldProc);
        }

        protected void GridProceduresCombo_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
        {
            RadComboBox combo = (RadComboBox)sender;
            string filter = e.Text;
            if (!string.IsNullOrEmpty(filter))
            {
                combo.Items.Clear();
                combo.DataSource = ProcedureManager.GetProceduresByFilter(filter);
                combo.DataBind();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            CurrentAdmission.AdmissionTime = Convert.ToInt32(txtAdmissionTime.Text);
            CurrentAdmission.AdmissionWeight = Convert.ToInt32(string.IsNullOrEmpty(txtAdmissionWeight.Text) ? "0" : txtAdmissionWeight.Text);
            CurrentAdmission.DaysAge = Convert.ToInt32(string.IsNullOrEmpty(txtAgeDays.Text) ? "0" : txtAgeDays.Text);
            CurrentAdmission.YearAge = Convert.ToInt32(string.IsNullOrEmpty(txtAgeYears.Text) ? "0" : txtAgeYears.Text);
            Sex admissionSex = (Sex)Enum.Parse(typeof(Sex), rcbGendre.SelectedValue);
            CurrentAdmission.Sex = (int)admissionSex;
            CurrentAdmission.DischargeType = Convert.ToByte(rcbDischargeType.SelectedValue);
            CurrentAdmission.MainDiagnostic = DiagnosticManager.GetDiagnosticByCode(rcbDiagnostics.SelectedValue);
            CurrentAdmission.MainProcedure = ProcedureManager.GetProcedureByCode(rcbProcedure.SelectedValue);

            ValidateAdmission();
        }

        private void ValidateAdmission()
        {
            StringBuilder builder = new StringBuilder();
            ValidationManager validationMan = new ValidationManager();
            ValidationResult result = validationMan.ClinicalValidation(this.CurrentAdmission);
            result.ClearSuccesResults();
            if (result.GetValidationResult() != ResultType.Success)
            {
                lvErrors.Visible = true;
                lblErrorTitle.Visible = true;
                lblLegendError.Visible = true;
                lblLegendWarning.Visible = true;
                lvErrors.DataSource = result.Items;
                lvErrors.DataBind();
            }
            else
            {
                GrouperManager grManager = new GrouperManager();
                GDRG groupingResult = grManager.GroupAdmissionToDRG(CurrentAdmission);
                if (groupingResult != null)
                {
                    GDRGServiceRead gdrgSrv = new GDRGServiceRead();
                    GDRG completeResult = gdrgSrv.GetDRGByCode(groupingResult.CodeDRG);

                    StringBuilder strbuilder = new StringBuilder();
                    strbuilder.AppendLine(String.Format("Grupa de diagnostic: {0}-{1}", completeResult.CodeDRG, completeResult.DisplayName));
                    strbuilder.AppendLine(String.Format("Valoare relativa: {0}", completeResult.RelativeValue));
                    lblResults.Text = strbuilder.ToString();
                    lblResults.Visible = true;
                    lblResult.Visible = true;
                }
                else
                {
                    //show some error
                }
            }
        }

        

        protected void rcbDiagnostics_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
        {
            RadComboBox combo = sender as RadComboBox;
            string filter = e.Text;
            if (!string.IsNullOrEmpty(filter))
            {
                combo.Items.Clear();
                combo.DataSource = DiagnosticManager.GetMainDiagnosticsByFilter(filter);
                combo.DataBind();
            } 
        }

        protected void rcbProcedure_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
        {
            RadComboBox combo = sender as RadComboBox;
            string filter = e.Text;
            if (!string.IsNullOrEmpty(filter))
            {
                rcbProcedure.Items.Clear();
                rcbProcedure.DataSource = ProcedureManager.GetProceduresByFilter(filter);
                rcbProcedure.DataBind();
            }
        }

        protected void lvErrors_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var dataItem = e.Item as ListViewDataItem;
            if (dataItem != null)
            {
                Label lblItem = dataItem.FindControl("lvItemLabel") as Label;
                if (lblItem != null)
                {
                    ResultType errorType = ((ValidationResult)(dataItem.DataItem)).Severity;
                    if (errorType == ResultType.Error)
                        lblItem.ForeColor = Color.Red;
                    else
                        if(errorType == ResultType.Warning)
                             lblItem.ForeColor = Color.Green;
                }
            }
        }
    }
}