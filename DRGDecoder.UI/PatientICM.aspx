﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PatientICM.aspx.cs"
    Inherits="DRGDecoder.UI.PatientICM" Culture="auto" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function SelectedIndexChanged(sender, args) 
            {
                var item = args.get_item();
                var val = item.get_value();
                var SenderId = sender.get_element().id;
                var txtDiagnostic = $find(SenderId.replace("gridDiagnosticsCombo", "txtDiagnostic"))
                txtDiagnostic.set_value(val);
        }
        function SelectedIndexChangedOnProc(sender, args)
        {
            var item = args.get_item();
            var val = item.get_value();
            var SenderId = sender.get_element().id;
            var txtDiagnostic = $find(SenderId.replace("gridProceduresCombo", "txtProcedure"))
            txtDiagnostic.set_value(val);
        }

        function ageDaysChanged(sender, args)
        {
            var txtYears = document.getElementById("<%=txtAgeYears.ClientID %>");
            txtYears.value = "0";
            document.getElementById("<%=rfvAdmissionWeight.ClientID%>").enabled = true;
            document.getElementById("<%=rfvAgeDays.ClientID%>").enabled = true;
            document.getElementById("<%=rfvAgeYears.ClientID%>").enabled = false;
        }

        function ageYearsChanges(sender, args)
        {
            var txtDays = document.getElementById("<%=txtAgeDays.ClientID %>");
            txtDays.value = "0";
            document.getElementById("<%=rfvAdmissionWeight.ClientID%>").enabled = false;
            document.getElementById("<%=rfvAgeDays.ClientID%>").enabled = false;
            document.getElementById("<%=rfvAgeYears.ClientID%>").enabled = true;
        }

        $(document).ready(function () {
            var listView = document.getElementById("<%=lblErrorTitle.ClientID%>");
            var divErrors = listView.parentElement;
                divErrors.style.visibility = listView.style.visibility;
            
        });
    </script>
    <div class="container col-sm-12 col-md-12">
    <div class="boxForm  col-sm-8 col-md-8">
        <div id="dataForm">
            <div class="col-sm-12 col-md-12 subTitle">
                <asp:Label runat="server" ID="lblPatientData" Text="Date pacient" meta:resourcekey="lblPatientDataResource"></asp:Label>
            </div>
            <div class="col-sm-12 col-md-12 padding">
                <div class="col-sm-6 col-md-6">
                    <asp:Label runat="server" ID="lblDiagnostic" Text="Diagnostic principal" meta:resourcekey="lblDiagnosticResource"></asp:Label>
                </div>
                <div class="col-sm-6 col-md-6 padding">
                    <asp:Label runat="server" ID="lblProcedure" Text="Procedura principala" meta:resourcekey="lblProcedureResource"></asp:Label>
                </div>
                </div>
            <div class="col-sm-12 col-md-12">
                <div class="col-sm-6 col-md-6 padding">
                   <telerik:RadComboBox runat="server" ID="rcbDiagnostics" AutoPostBack="true" Width="100%"  EnableVirtualScrolling="true"
                       DataValueField="Code" DataTextField="DisplayName" EnableLoadOnDemand="true" AllowCustomText="true" 
                       OnItemsRequested="rcbDiagnostics_ItemsRequested" DropDownAutoWidth="Enabled" meta:resourcekey="rcbDiagnosticsResource"
                       EmptyMessage ="Cautati dupa Cod sau Nume" OnSelectedIndexChanged="RcbDiagnostics_SelectedIndexChanged"
                       >
                   </telerik:RadComboBox>
                    <asp:RequiredFieldValidator ID="rfvDiagnostic" runat="server" ErrorMessage="*" ToolTip="Required field" 
                        ControlToValidate="rcbDiagnostics" meta:resourcekey="rfvResource">
                    </asp:RequiredFieldValidator>
                    </div>
                 <div class ="col-sm-6 col-md-6 padding">
                    <telerik:RadComboBox runat="server" ID="rcbProcedure" AutoPostBack="true" Width="100%"  EnableVirtualScrolling="true"
                        DataValueField="Code" DataTextField="DisplayName" meta:resourcekey="rcbProcedureResource" AllowCustomText="true"
                        EnableLoadOnDemand="true" OnItemsRequested="rcbProcedure_ItemsRequested" DropDownAutoWidth="Enabled"
                        EmptyMessage="Cautati dupa Cod sau Nume" OnSelectedIndexChanged="RcbProcedure_SelectedIndexChanged"
                        >
                        </telerik:RadComboBox>
                </div>
            </div>
            <div class="col-sm-12 col-md-12">
                  <div class="col-sm-6 col-md-6 padding">
                      <asp:Label ID="lblSecondaryDiagnostics" runat="server" Text="Diagnostice secundare" meta:resourcekey="lblSecondaryDiagnosticsResource"></asp:Label>
                      </div>
                </div>
            <div class="col-sm-12 col-md-12">
                <div class="col-sm-11 col-md-11 padding">
                    <telerik:RadGrid ID="rgSecondaryDiagn" runat="server"
                        OnNeedDataSource="RgSecondaryDiagn_NeedDataSource" 
                        OnInsertCommand="RgSecondaryDiagn_InsertCommand"
                        OnUpdateCommand="RgSecondaryDiagn_UpdateCommand"
                        OnDeleteCommand="RgSecondaryDiagn_DeleteCommand"
                        AutoGenerateColumns="false" AutoGenerateEditColumn="true" AutoGenerateDeleteColumn="true">
                        <MasterTableView CommandItemSettings-ShowRefreshButton="false" EditMode="InPlace" Width="100%"
                            CommandItemSettings-AddNewRecordText="Adauga"  CommandItemSettings-ShowAddNewRecordButton ="true"
                            ShowHeadersWhenNoRecords="true" CommandItemDisplay="Top" meta:resourcekey="rgSecondaryDiagnResource"
                            DataKeyNames="Id">
                            <Columns>
                                <telerik:GridBoundColumn UniqueName="Id" DataField="Id" Visible="false"></telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn UniqueName="DisplayName" HeaderText="Denumire" DataField="DisplayName" 
                                    HeaderStyle-HorizontalAlign="Center" meta:resourcekey="DisplayNameResource">
                                    <EditItemTemplate>
                                       <telerik:RadComboBox runat="server" ID="gridDiagnosticsCombo" AutoPostBack="true" Width="100%"
                                           DataValueField="Code" DataTextField="DisplayName" EnableLoadOnDemand="true" EnableVirtualScrolling="true"
                                           AllowCustomText="true" OnItemsRequested="gridDiagnosticsCombo_ItemsRequested" DropDownAutoWidth="Enabled"
                                           EmptyMessage ="Cautati dupa Cod sau Nume" OnClientSelectedIndexChanged="SelectedIndexChanged"
                                           meta:resourcekey="gridDiagnosticsComboResource">
                                       </telerik:RadComboBox> 
                                        <asp:RequiredFieldValidator ID="rfvSecDiagn" runat="server" ErrorMessage="*" ToolTip="Required field" 
                                                ControlToValidate="gridDiagnosticsCombo" meta:resourcekey="rfvResource">
                                            </asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <%# Eval("DisplayName") %>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn UniqueName="Code" HeaderText="Cod" DataField="Code" meta:resourcekey="CodeResource"
                                     HeaderStyle-HorizontalAlign="Center">
                                    <EditItemTemplate>
                                        <telerik:RadTextBox ID="txtDiagnostic" runat="server" ReadOnly="true" Text='<%# Eval("Code") %>'
                                            ></telerik:RadTextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                         <%# Eval("Code") %>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>
                        </MasterTableView>
                        <ValidationSettings CommandsToValidate="PerformInsert,Update"></ValidationSettings>
                    </telerik:RadGrid>
                </div>
            </div>
            <div class="col-sm-12 col-md-12">
                <div class="col-sm-6 col-md-6 padding">
                    <asp:Label ID="lblSecondaryProcedures" runat="server" Text="Proceduri secundare" meta:resourcekey="lblSecondaryProceduresResource"></asp:Label>
                </div>
            </div>
             <div class="col-sm-12 col-md-12">
                <div class="col-sm-11 col-md-11 padding">
                    <telerik:RadGrid ID="RgSecondaryProc" runat="server"
                        OnNeedDataSource="RgSecondaryProc_NeedDataSource" 
                        OnInsertCommand="RgSecondaryProc_InsertCommand"
                        OnUpdateCommand="RgSecondaryProc_UpdateCommand"
                        OnDeleteCommand="RgSecondaryProc_DeleteCommand"
                        AutoGenerateColumns="false" AutoGenerateEditColumn="true" AutoGenerateDeleteColumn="true">
                        <MasterTableView CommandItemSettings-ShowRefreshButton="false" EditMode="InPlace" Width="100%"
                            CommandItemSettings-AddNewRecordText="Adauga"  CommandItemSettings-ShowAddNewRecordButton ="true"
                            ShowHeadersWhenNoRecords="true" CommandItemDisplay="Top" meta:resourcekey="RgSecondaryProcResource"
                            DataKeyNames="Id">
                            <Columns>
                                <telerik:GridBoundColumn UniqueName="Id" DataField="Id" Visible="false"></telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn UniqueName="DisplayName" HeaderText="Denumire" DataField="DisplayName" 
                                    HeaderStyle-HorizontalAlign="Center" meta:resourcekey="ProcDisplayNameResource">
                                    <EditItemTemplate>
                                       <telerik:RadComboBox runat="server" ID="gridProceduresCombo" AutoPostBack="true" Width="100%"
                                           DataValueField="Code" DataTextField="DisplayName" EnableLoadOnDemand="true" EnableVirtualScrolling="true"
                                           AllowCustomText="true" OnItemsRequested="GridProceduresCombo_ItemsRequested" DropDownAutoWidth="Enabled"
                                           EmptyMessage ="Cautati dupa Cod sau Nume" OnClientSelectedIndexChanged="SelectedIndexChangedOnProc"
                                           meta:resourcekey="gridProceduresComboResource">
                                       </telerik:RadComboBox> 
                                        <asp:RequiredFieldValidator ID="rfvSecProc" runat="server" ErrorMessage="*" ToolTip="Required field" 
                                                ControlToValidate="gridProceduresCombo" meta:resourcekey="rfvResource">
                                            </asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <%# Eval("DisplayName") %>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn UniqueName="Code" HeaderText="Cod" DataField="Code" meta:resourcekey="ProcCodeResource"
                                     HeaderStyle-HorizontalAlign="Center">
                                    <EditItemTemplate>
                                        <telerik:RadTextBox ID="txtProcedure" runat="server" ReadOnly="true" Text='<%# Eval("Code") %>'
                                            ></telerik:RadTextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                         <%# Eval("Code") %>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 ">
                <div class="col-sm-6 col-md-6 padding">
                    <asp:Label runat="server" ID="lblAgeYears" Text="Varsta ani" meta:resourcekey="lblAgeYearsResource"></asp:Label>
                </div>
                <div class="col-sm-6 col-md-6 padding">
                    <asp:Label runat="server" ID="lblAgeDays" Text="Varsta zile (doar pentru nou nascuti)" meta:resourcekey="lblAgeDaysResource"></asp:Label>
                </div>
                </div>
            <div class="col-sm-12 col-md-12 ">
                <div class="col-sm-6 col-md-6 padding">
                    <telerik:RadNumericTextBox ID="txtAgeYears" runat="server" AllowOutOfRangeAutoCorrect="true" MinValue="0" MaxValue="124"
                        ClientEvents-OnValueChanged="ageYearsChanges">
                        <NumberFormat DecimalDigits="0" GroupSeparator="" />
                    </telerik:RadNumericTextBox>
                     <asp:RequiredFieldValidator ID="rfvAgeYears" runat="server" ErrorMessage="*" ToolTip="Required field" 
                        ControlToValidate="txtAgeYears" meta:resourcekey="rfvResource" Enabled="true">
                    </asp:RequiredFieldValidator>
                </div>
                <div class="col-sm-6 col-md-6 padding">
                    <telerik:RadNumericTextBox ID="txtAgeDays" runat="server" AllowOutOfRangeAutoCorrect="true" MinValue="0" MaxValue="365" 
                        ClientEvents-OnValueChanged="ageDaysChanged">
                        <NumberFormat DecimalDigits="0" GroupSeparator=""/>
                    </telerik:RadNumericTextBox>
                      <asp:RequiredFieldValidator ID="rfvAgeDays" runat="server" ErrorMessage="*" ToolTip="Required field" 
                        ControlToValidate="txtAgeDays" meta:resourcekey="rfvResource" Enabled="false">
                    </asp:RequiredFieldValidator>
                </div>
                </div>
            <div class="col-sm-12 col-md-12 ">
                <div class="col-sm-6 col-md-6 padding">
                    <asp:Label runat="server" ID="lblAdmissionWeight" Text="Greutate la internare (grame)" meta:resourcekey="lblAdmissionWeightResource"></asp:Label>
                    
                </div>
                <div class="col-sm-6 col-md-6 padding">
                    <asp:Label runat="server" ID="lblGendre" Text="Sex" meta:resourcekey="lblGendreResource"></asp:Label>
                </div>
                </div>
            <div class="col-sm-12 col-md-12 ">
                <div class="col-sm-6 col-md-6 padding">
                   <telerik:RadNumericTextBox ID="txtAdmissionWeight" runat="server" MinValue="400" MaxValue="9999" AllowOutOfRangeAutoCorrect="true">
                        <NumberFormat DecimalDigits="0" GroupSeparator=""/>
                   </telerik:RadNumericTextBox>
                    <asp:RequiredFieldValidator ID="rfvAdmissionWeight" runat="server" ErrorMessage="*" ToolTip="Required field" 
                        ControlToValidate="txtAdmissionWeight" meta:resourcekey="rfvResource" Enabled="false">
                    </asp:RequiredFieldValidator>
                </div>
                <div class="col-sm-6 col-md-6 padding">
                    <telerik:RadComboBox ID="rcbGendre" runat="server"> </telerik:RadComboBox>
                    <asp:RequiredFieldValidator ID="rfvGender" runat="server" ErrorMessage="*" ToolTip="Required field" 
                        ControlToValidate="rcbGendre" meta:resourcekey="rfvResource" >
                    </asp:RequiredFieldValidator>
                </div>
                </div>
             <div class="col-sm-12 col-md-12 ">
                <div class="col-sm-6 col-md-6 padding">
                    <asp:Label runat="server" ID="lblAdmissionTime" Text="Zile spitalizare" meta:resourcekey="lblAdmissionTimeResource"></asp:Label>
                </div>
                <div class="col-sm-6 col-md-6 padding">
                    <asp:Label runat="server" ID="lblDischargeType" Text="Tip externare" meta:resourcekey="lblDischargeTypeResource"></asp:Label>
                </div>
                </div>
             <div class="col-sm-12 col-md-12 ">
                <div class="col-sm-6 col-md-6 padding">
                  <telerik:RadNumericTextBox runat="server" ID="txtAdmissionTime" MinValue="1" MaxValue="99999" AllowOutOfRangeAutoCorrect="true">
                      <NumberFormat DecimalDigits="0" GroupSeparator=""/>
                  </telerik:RadNumericTextBox>
                    <asp:RequiredFieldValidator ID="rfvAdmissionTime" runat="server" ErrorMessage="*" ToolTip="Required field" 
                        ControlToValidate="txtAdmissionTime" meta:resourcekey="rfvResource">
                    </asp:RequiredFieldValidator>
                </div>
                <div class="col-sm-6 col-md-6 padding">
                    <telerik:RadComboBox ID="rcbDischargeType" runat="server" DataValueField="Id" DataTextField="DisplayName">
                    </telerik:RadComboBox>
                    <asp:RequiredFieldValidator ID="rfvDiaschargeType" runat="server" ErrorMessage="*" ToolTip="Required field" 
                        ControlToValidate="rcbDischargeType" meta:resourcekey="rfvResource">
                    </asp:RequiredFieldValidator>
                </div>
                </div>
            <div class="col-sm-12 col-md-12 ">
                  <div class="col-sm-6 col-md-6 padding">
                      <telerik:RadButton ID="btnSubmit" runat="server" AutoPostBack="true" OnClick="btnSubmit_Click" Text="Calculeaza" 
                          meta:resourcekey="btnSubmitResource"></telerik:RadButton>
                      </div>
                </div>

        </div>
    </div>
   <div class="col-sm-4 col-md-4" ID="divErrors" style="white-space:pre-line; visibility:hidden;">
       <div class="row errorsBox">
            <asp:Label ID="lblErrorTitle" runat="server" Text="Erori de validare" ForeColor="Red" Visible="false" Style=" text-decoration:underline">
            </asp:Label>
            <asp:ListView ID="lvErrors" runat="server" DataKeyNames="Result" OnItemDataBound="lvErrors_ItemDataBound">
                <ItemTemplate>
                    <asp:Label ID="lvItemLabel" runat="server" Text=<%#Eval("Message")%> ></asp:Label>
                </ItemTemplate>
            </asp:ListView>
       <div style="white-space:nowrap; text-decoration:overline"">
          <asp:Label ID="lblLegendError" runat ="server" Text="Error" meta:resourcekey="lblLegendErrorResource" Visible="false" ForeColor="Red">
          </asp:Label>
          <asp:Label ID="lblLegendWarning" runat="server" Text="Warning" meta:resourcekey="lblLegendWarningResource" Visible="false" ForeColor="Green">
          </asp:Label>
       </div>
     </div>
       <div class="row errorBox" id="divGroupingResult" style="white-space:pre-line; visibility:hidden;">
           <asp:Label ID="lblResult" runat="server" Text="Rezultat grupare" Font-Bold="true"></asp:Label>
           <asp:Label ID="lblResults" runat="server"></asp:Label>
       </div>
   </div>
   
</div>
</asp:Content>
